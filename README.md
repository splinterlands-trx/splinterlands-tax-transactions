# Splinterlands Tax Transactions

*** This is a very beta application. It has been quickly put together for 2021 tax season to compensate for some
Splinterlands CSV file shortcomings.

*** USE AT YOUR OWN RISK. YOU HAVE TO CONFIRM THE ACCURACY OF THE RESULTS.

# Basic usage:

SplintTaxTrxApplication playerName [CryptoTaxService] [CSV_filename]

playerName: the name used to play splinterlands.

CryptoTaxService: the service helping in preparing crypto transactions for tax filings. Only Coinpanda.io is supported
at this time.

CSV_Filename: the file you get from Splinterlands - splinterlands-balances.csv To get the file just log in to the game
on desktop, click on your avatar name -> balance history -> export -> export all 2021 history.

# How it works

1. This application loads all transactions form the CSV provided in to a database.
2. Next it pulls all transactions from Hive, https://api.hive.blog, based on the playerName provided. Unfortunately many
   of these transactions are request to do some action, not completed successful transactions.
3. Next it tries to match the transactions in the database with those from Hive and update them accordingly. In many
   cases, it just adds the TxHash. For NFTs it will add the card identifier as well. (The CSV only gives you the player
   selling the card.)
4. For Combining card, it tries to sell all the card involved for 0 DEC, and then buy the card they were merged into for
   the combined total of cards involved when they were last bought prior to combining cards.
5. Next the application stores all the new transactions in the database. Unmatched transactions are recorded and may
   duplicate other transaction or not have been successful. The user has to manually fix this at this time.
6. Last the applications pulls all transactions form the database and writes the SplinterlandsCoinpanda.csv witch is in
   Coinpanda format.
7. There will be a log created under /logs.

# Issues

This application only knows and processes transaction types that have been used with my account. There are many more
types and I will need details on the transactions to implement them. Please leave an issue with the transaction type and
a TrxHash I can look up.

As the transactions that can be pulled are in a request phase, not the completed final phase, some information is
incorrect. A request to rent a card may not go through thus does not need to be recorded. A purchase of a card may not
go through. When this happens and the player has bought multiple cards in the same transaction, it will be unlikely the
matching process will match the cards that did succeed.

I have a feedback request for better tax reporting: https://feedback.splinterlands.com/1037 Please upvote.

# What to do after the application runs

1. Open the SplinterlandsCoinpanda.csv in an editor. (Notepad if nothing else, but Excel or similar would be much
   better. Most do CSV imports.)
2. Find the last transaction inputted by splinterlands-balances.csv. Usually near the 2021-12-31 date. The initial rows
   all come form the splinterlands-balances.csv file. Many will have been updated.
3. The remaining row are transaction that did not get matched.
4. Most of the sm_market_rent and sm_stake_tokens are failed transactions and can be removed.
5. The plain NFT transaction have to be manually synced. Prices are in USD not DEC. You will have to get the correct DEC
   value from a matching transaction on or near the date and update it in the new transaction and then remove the
   transaction you matched. Note most likely you will find 1 or more transactions that can not be matched and should be
   removed.
6. The NFT Combine transactions, are as described above. All cards to be combined are sold for 0 DEC, and then the card
   they are merged into is purchased for total DEC used when the cards used for combining were last purchased. This
   application can not handle cards bought with other currencies other than DEC. Also, some cards may have been
   gifted/rewarded to the player and thus have no DEC cost to start with. The application should report when a prior
   match to a combined card is not found.


If any of this was helpful, please use my referral link: https://coinpanda.io/?ref=2799fe2b6af6 Signup is free.