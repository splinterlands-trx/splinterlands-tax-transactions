package com.zorner.SplintTaxTrx.splinttaxtrx.transformers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zorner.SplintTaxTrx.splinttaxtrx.interfaces.SplintTrxInerface;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.HistoryTrx;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.Transaction;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.CryptoTransaction;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint.*;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint.componets.Pack;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.enums.SplintOpType;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.responses.HistoryResponse;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.responses.TransactionResponse;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.responses.componets.HistoryMapEntryResponse;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class HiveTrxTransformer {
    
    private final ObjectMapper objectMapper = new ObjectMapper();
    
    public List<CryptoTransaction> getCryptoTransactions(HistoryResponse historyResponse) {
        return getCryptoTransactions(getHistoryTransactions(historyResponse));
    }
    
    public <SplintDetail extends AbstractSplint> List<CryptoTransaction> getCryptoTransactions(@NonNull List<HistoryTrx<SplintDetail>> history) {
        System.out.println("Preparing history transactions for import into database. This make a few minutes as transaction details maybe requested from hive for individual transactions.");
        log.info("Preparing history transactions for import into database.");
        List<CryptoTransaction> trxes = new ArrayList<>();
        SplintTrxInerface splintDetail;
        for (HistoryTrx<SplintDetail> trx : history) {
            if (trx.getDetails() instanceof SplintTrxInerface) {
                splintDetail = (SplintTrxInerface) trx.getDetails();
                trxes.addAll(splintDetail.getCryptoDetail(new CryptoTransaction().toBuilder()
                        .date(trx.getTimestamp())
                        .trxHash(trx.getTxrId())
                        .splintType(trx.getSplintOpType())
                        .build()));
            }
        }
        System.out.println("History transactions ready for import in the database.");
        log.info("History transactions ready for import in the database.");
        return trxes;
    }
    
    public List<HistoryTrx<AbstractSplint>> getHistoryTransactions(@NonNull HistoryResponse historyResponse) {
        List<HistoryTrx<AbstractSplint>> trxes = new ArrayList<>();
        for (HistoryMapEntryResponse mapEntry : historyResponse.getResult().getHistory()) {
            SplintOpType splintOpType = SplintOpType.valueOfLabel(mapEntry.getTrx().getOp().getValue().getId());
            if (splintOpType == null) {
                log.warn("New Splinterlands transaction type: " + mapEntry.getTrx().getOp().getValue().getId());
            }
            trxes.add(HistoryTrx.builder()
                    .txrId(mapEntry.getTrx().getTrxId())
                    .block(mapEntry.getTrx().getBlock())
                    .trxInBlock(mapEntry.getTrx().getTrxInBlock())
                    .opInTrx(mapEntry.getTrx().getOpInTrx())
                    .virtualOp(mapEntry.getTrx().isVirtualOp())
                    .timestamp(mapEntry.getTrx().getTimestamp())
                    .opType(mapEntry.getTrx().getOp().getType())
                    .requiredAuths(mapEntry.getTrx().getOp().getValue().getRequiredAuths())
                    .requiredPostingAuths(mapEntry.getTrx().getOp().getValue().getRequiredPostingAuths())
                    .splintOpType(splintOpType)
                    .details(getDetails(mapEntry.getTrx().getOp().getValue().getId(), mapEntry.getTrx().getOp().getValue().getJson()))
                    .operationId(mapEntry.getTrx().getOperationId())
                    .build());
            
        }
        return trxes;
    }
    
    public Transaction<AbstractSplint> getTransaction(@NonNull TransactionResponse transactionResponse) {
        if (transactionResponse.getResult().getOperations().size() > 1) {
            log.warn("Transaction: " + transactionResponse.getResult().getTransactionNum() + " has more than 1 operation in it. Handling of a transaction with multiple operations not enabled.");
            return null;
        }
        SplintOpType splintOpType = SplintOpType.valueOfLabel(transactionResponse.getResult().getOperations().get(0).getValue().getId());
        if (splintOpType == null) {
            log.warn("New Splinterlands transaction type: " + transactionResponse.getResult().getOperations().get(0).getValue().getId());
        }
        return Transaction.builder()
                .refBlockNum(transactionResponse.getResult().getRefBlockNum())
                .refBlockPrefix(transactionResponse.getResult().getRefBlockPrefix())
                .expiration(transactionResponse.getResult().getExpiration())
                .opType(transactionResponse.getResult().getOperations().get(0).getType())
                .requiredAuths(transactionResponse.getResult().getOperations().get(0).getValue().getRequiredAuths())
                .requiredPostingAuths(transactionResponse.getResult().getOperations().get(0).getValue().getRequiredPostingAuths())
                .splintOpType(splintOpType)
                .details(getDetails(transactionResponse.getResult().getOperations().get(0).getValue().getId(), transactionResponse.getResult().getOperations().get(0).getValue().getJson()))
                .signatures(transactionResponse.getResult().getSignatures())
                .transactionId(transactionResponse.getResult().getTransactionId())
                .block(transactionResponse.getResult().getBlockNum())
                .trxInBlock(transactionResponse.getResult().getTransactionNum())
                .build();
    }
    
    public AbstractSplint getDetails(String trxType, String json) {
        SplintOpType type = SplintOpType.valueOfLabel(trxType);
        if (type == null) {
            log.warn("Using START_QUEST as dummy Splinterlands transaction type. Maybe new Splinterlands transaction type encountered.");
            return null;
        }
        AbstractSplint details = null;
        try {
            switch (type) {
                case ADD_WALLET:
                    details = objectMapper.readValue(json, AddWallet.class);
                    break;
                case CLAIM_REWARD:
                    details = objectMapper.readValue(json, CliamReward.class);
                    break;
                case COMBINE_CARDS:
                    details = objectMapper.readValue(json, CombineCards.class);
                    break;
                case ENTER_TOURNAMENT:
                    details = objectMapper.readValue(json, EnterTournament.class);
                    break;
                case MARKET_CANCEL_RENTAL:
                    details = objectMapper.readValue(json, MarketCancelRental.class);
                    break;
                case MARKET_LIST:
                    details = objectMapper.readValue(json, MarketList.class);
                    break;
                case MARKET_PURCHASE:
                    details = objectMapper.readValue(json, MarketPurchase.class);
                    break;
                case MARKET_RENT:
                    details = objectMapper.readValue(json, MarketRent.class);
                    break;
                case REFRESH_QUEST:
                    details = objectMapper.readValue(json, RefreshQuest.class);
                    break;
                case SELL_CARDS:
                    details = new SellCards().toBuilder()
                            .packs(objectMapper.readValue(json, objectMapper.getTypeFactory()
                                    .constructCollectionType(List.class, Pack.class)))
                            .build();
                    break;
                case SET_AUTHORITY:
                    details = objectMapper.readValue(json, SetAuthority.class);
                    break;
                case STAKE_TOKENS:
                    details = objectMapper.readValue(json, StakeTokens.class);
                    break;
                case START_QUEST:
                    details = objectMapper.readValue(json, StartQuest.class);
                    break;
                case TOURNAMENT_CHECK_IN:
                    details = objectMapper.readValue(json, TournamentCheckIn.class);
                    break;
                case UPDATE_PRICE:
                    details = objectMapper.readValue(json, UpdatePrice.class);
                    break;
                default:
                    log.warn(trxType + " transaction type not found for JSON deserialization.");
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return details;
    }
}
