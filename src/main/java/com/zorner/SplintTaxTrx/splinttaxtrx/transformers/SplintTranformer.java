package com.zorner.SplintTaxTrx.splinttaxtrx.transformers;

import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.CryptoTransaction;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.SplintBalanceTrx;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.enums.CoinpandaTrxLabel;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.enums.CoinpandaTrxType;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.enums.SplintBalanceType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class SplintTranformer {
    public List<CryptoTransaction> getTransactions(List<SplintBalanceTrx> splintTrxes) {
        List<CryptoTransaction> trxes = new ArrayList<>();
        for (SplintBalanceTrx trx : splintTrxes) {
            SplintBalanceType type = SplintBalanceType.valueOfLabel(trx.getType());
            if (type == null) {
                log.warn(trx.getType() + " transaction type not found for CSV.");
                continue;
            }
            CryptoTransaction cryptoTrx = new CryptoTransaction().toBuilder()
                    .date(trx.getDate())
                    .type(type.getType())
                    .label(type.getCoinpandaLabel())
                    .description(trx.getFromTo())
                    .splintType(type.getSplintType())
                    .build();
            if (type.getType().equals(CoinpandaTrxType.SEND)) {
                cryptoTrx.setSentAmount(trx.getAmount().multiply(BigDecimal.valueOf(-1)));
                cryptoTrx.setSentCurrency(trx.getToken());
            }
            if (type.getType().equals(CoinpandaTrxType.RECEIVE)) {
                cryptoTrx.setReceivedAmount(trx.getAmount());
                cryptoTrx.setReceivedCurrency(trx.getToken());
            }
            if (type.getType().equals(CoinpandaTrxType.TRANSFER)) {
                if (trx.getAmount().compareTo(BigDecimal.valueOf(0)) < 0) {
                    cryptoTrx.setSentAmount(trx.getAmount().multiply(BigDecimal.valueOf(-1)));
                    cryptoTrx.setSentCurrency(trx.getToken());
                    cryptoTrx.setType(CoinpandaTrxType.SEND);
                } else {
                    cryptoTrx.setReceivedAmount(trx.getAmount());
                    cryptoTrx.setReceivedCurrency(trx.getToken());
                    cryptoTrx.setType(CoinpandaTrxType.RECEIVE);
                }
            }
            if (type.getType().equals(CoinpandaTrxType.TRADE)) {
                if (trx.getAmount().compareTo(BigDecimal.valueOf(0)) < 0) {
                    cryptoTrx.setSentAmount(trx.getAmount().multiply(BigDecimal.valueOf(-1)));
                    cryptoTrx.setSentCurrency(trx.getToken());
                    if (type.getCoinpandaLabel().equals(CoinpandaTrxLabel.NFT)) {
                        cryptoTrx.setReceivedAmount(BigDecimal.valueOf(1));
                        cryptoTrx.setReceivedCurrency(trx.getFromTo());
                    } else
                        cryptoTrx.setType(CoinpandaTrxType.SEND);
                } else {
                    cryptoTrx.setReceivedAmount(trx.getAmount());
                    cryptoTrx.setReceivedCurrency(trx.getToken());
                    if (type.getCoinpandaLabel().equals(CoinpandaTrxLabel.NFT)) {
                        cryptoTrx.setSentAmount(BigDecimal.valueOf(1));
                        cryptoTrx.setSentCurrency(trx.getFromTo());
                    } else
                        cryptoTrx.setType(CoinpandaTrxType.RECEIVE);
                }
            }
            trxes.add(cryptoTrx);
        }
        return trxes;
    }
}
