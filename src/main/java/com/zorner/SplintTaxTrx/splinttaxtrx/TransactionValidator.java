package com.zorner.SplintTaxTrx.splinttaxtrx;

import com.zorner.SplintTaxTrx.splinttaxtrx.interfaces.ConfirmTransaction;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.CryptoTransaction;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.enums.SplintOpType;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class TransactionValidator implements ConstraintValidator<ConfirmTransaction, CryptoTransaction> {
    
    @Override
    public boolean isValid(CryptoTransaction trx, ConstraintValidatorContext constraintValidatorContext) {
        switch (trx.getType()) {
            case TRADE:
                if (trx.getSplintType() == SplintOpType.COMBINE_CARDS && trx.getDescription().startsWith("Combine purchase "))
                    return trx.getSentCurrency() != null &&
                            trx.getReceivedAmount() != null && trx.getReceivedCurrency() != null;
                else
                    return trx.getSentAmount() != null && trx.getSentCurrency() != null &&
                            trx.getReceivedAmount() != null && trx.getReceivedCurrency() != null;
            case SELL:
            case BUY:
            case TRANSFER:
                return trx.getSentAmount() != null && trx.getSentCurrency() != null &&
                        trx.getReceivedAmount() != null && trx.getReceivedCurrency() != null;
            case SEND:
//            case WITHDRAW:
                return trx.getSentAmount() != null && trx.getSentCurrency() != null &&
                        trx.getReceivedAmount() == null && trx.getReceivedCurrency() == null;
            case RECEIVE:
//            case DEPOSIT:
                return trx.getSentAmount() == null && trx.getSentCurrency() == null &&
                        trx.getReceivedAmount() != null && trx.getReceivedCurrency() != null;
            default:
                return false;
        }
    }
}
