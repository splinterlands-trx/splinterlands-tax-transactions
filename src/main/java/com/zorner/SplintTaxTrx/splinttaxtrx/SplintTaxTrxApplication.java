package com.zorner.SplintTaxTrx.splinttaxtrx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SplintTaxTrxApplication {

	public static void main(String[] args) {
        
        SpringApplication.run(SplintTaxTrxApplication.class, args);
    }
}
