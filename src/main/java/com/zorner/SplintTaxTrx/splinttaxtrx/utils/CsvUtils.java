package com.zorner.SplintTaxTrx.splinttaxtrx.utils;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

@Slf4j
@Component
public class CsvUtils {
    
    public <CvsEntity> List<CvsEntity> getCsvList(Class<CvsEntity> cvsEntityClassType, String fileName) {
        return getCsvList(cvsEntityClassType, fileName, false, ',');
    }
    
    public <CvsEntity> List<CvsEntity> getCsvList(Class<CvsEntity> cvsEntityClassType, String fileName, boolean noHeaders) {
        return getCsvList(cvsEntityClassType, fileName, noHeaders, ',');
    }
    
    public <CvsEntity> List<CvsEntity> getCsvList(Class<CvsEntity> cvsEntityClassType, String fileName, char separator) {
        return getCsvList(cvsEntityClassType, fileName, false, separator);
    }
    
    public <CvsEntity> List<CvsEntity> getCsvList(Class<CvsEntity> cvsEntityClassType, String fileName, boolean noHeaders, char separator) {
        try {
            File file = new File(fileName);
            CsvMapper mapper = new CsvMapper();
            CsvSchema csvSchema = mapper.schemaFor(cvsEntityClassType)
                    .withColumnSeparator(separator);
            
            if (noHeaders) {
                csvSchema = csvSchema.withoutHeader();
                mapper.enable(CsvParser.Feature.WRAP_AS_ARRAY);
            } else {
                csvSchema = csvSchema.withHeader();
            }
            
            mapper.enable(CsvParser.Feature.TRIM_SPACES);
            MappingIterator<CvsEntity> readValues = mapper
                    .readerFor(cvsEntityClassType)
                    .with(csvSchema)
                    .readValues(file);
            return readValues.readAll();
        } catch (Exception e) {
            log.error("Error occurred while loading object list from file " + fileName, e);
            return Collections.emptyList();
        }
    }
    
    public <CvsEntity> void writeCsvList(Class<CvsEntity> cvsEntityClassType, List<CvsEntity> entity, String fileName) {
        writeCsvList(cvsEntityClassType, entity, fileName, false, ',');
    }
    
    public <CvsEntity> void writeCsvList(Class<CvsEntity> cvsEntityClassType, List<CvsEntity> entity, String fileName, boolean noHeaders) {
        writeCsvList(cvsEntityClassType, entity, fileName, noHeaders, ',');
    }
    
    public <CvsEntity> void writeCsvList(Class<CvsEntity> cvsEntityClassType, List<CvsEntity> entity, String fileName, char separator) {
        writeCsvList(cvsEntityClassType, entity, fileName, false, separator);
    }
    
    public <CvsEntity> void writeCsvList(Class<CvsEntity> cvsEntityClassType, List<CvsEntity> entityList, String fileName, boolean noHeaders, char separator) {
        
        CsvMapper mapper = new CsvMapper();
        CsvSchema csvSchema = mapper.schemaFor(cvsEntityClassType)
                .withColumnSeparator(separator);
        
        if (noHeaders) {
            csvSchema = csvSchema.withoutHeader();
            mapper.enable(CsvParser.Feature.WRAP_AS_ARRAY);
        } else {
            csvSchema = csvSchema.withHeader()
                    .withColumnReordering(true)
                    .withoutQuoteChar();
        }
        
        mapper.enable(CsvParser.Feature.TRIM_SPACES);
        mapper.enable(CsvParser.Feature.SKIP_EMPTY_LINES);
        
        try {
            mapper.writer(csvSchema)
                    .writeValue(new File(fileName), entityList);
        } catch (IOException e) {
            log.error("jackson to csv file error, path: {}, separator: {}, list: {}", fileName, separator, entityList, e);
        }
    }
}
