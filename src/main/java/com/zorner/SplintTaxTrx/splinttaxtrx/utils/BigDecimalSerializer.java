package com.zorner.SplintTaxTrx.splinttaxtrx.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.math.BigDecimal;

public class BigDecimalSerializer extends StdSerializer<BigDecimal> {
  
  public BigDecimalSerializer() {
    this(null);
  }
  
  public BigDecimalSerializer(Class<BigDecimal> c) {
    super(c);
  }
  
  @Override
  public void serialize(BigDecimal bigDecimal, JsonGenerator generator, SerializerProvider provider) throws IOException {
    generator.writeNumber(new BigDecimal(bigDecimal.stripTrailingZeros().toPlainString()));
  }
}
