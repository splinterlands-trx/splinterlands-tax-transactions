package com.zorner.SplintTaxTrx.splinttaxtrx.models;

import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint.AbstractSplint;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.enums.SplintOpType;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.List;

@Builder
@Getter
public class Transaction<SplintDetail extends AbstractSplint> {
    private long refBlockNum;
    private long refBlockPrefix;
    private LocalDateTime expiration;
    private String opType;
    private List<String> requiredAuths;
    private List<String> requiredPostingAuths;
    private SplintOpType splintOpType;
    private SplintDetail details;
    private List<String> signatures;
    private String transactionId;
    private long block;
    private int trxInBlock;
}
