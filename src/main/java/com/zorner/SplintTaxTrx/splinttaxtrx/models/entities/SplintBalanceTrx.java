package com.zorner.SplintTaxTrx.splinttaxtrx.models.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@JsonPropertyOrder({"token", "type", "fromTo", "amount", "balance", "date"})
public class SplintBalanceTrx {
    @JsonProperty("Token")
    private String token;
    @JsonProperty("Type")
    private String type;
    @JsonProperty("From/To")
    private String fromTo;
    @JsonProperty("Amount")
    private BigDecimal amount;
    @JsonProperty("Balance")
    private BigDecimal balance;
    @JsonProperty("Created Date")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime date;
}
