package com.zorner.SplintTaxTrx.splinttaxtrx.models.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum CrytpoTaxService {
    COINPANDA("Coinpanda", "https://coinpanda.io/");
    private static final Map<String, CrytpoTaxService> BY_LABEL = new HashMap<>();
    private static final Map<String, CrytpoTaxService> BY_WEB = new HashMap<>();
    
    static {
        for (CrytpoTaxService element : values()) {
            BY_LABEL.put(element.label, element);
            BY_WEB.put(element.web, element);
        }
    }
    
    private final String label;
    private final String web;
    
    CrytpoTaxService(String label, String web) {
        this.label = label;
        this.web = web;
    }
    
    public static CrytpoTaxService valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }
    
    public static CrytpoTaxService valueOfWeb(String web) {
        return BY_WEB.get(web);
    }
    
    @Override
    public String toString() {
        return this.label;
    }
    
    @JsonValue
    public String getLabel() {
        return this.label;
    }
    
    public String getWeb() {
        return this.web;
    }
}
