package com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint.componets;

import lombok.Getter;

@Getter
public class SplintItem {
    private String trxHash;
    private int index;
    
    public SplintItem(String item) {
        String[] itemSplit = item.split("-");
        this.trxHash = itemSplit[0];
        this.index = Integer.parseInt(itemSplit[1]);
    }
}
