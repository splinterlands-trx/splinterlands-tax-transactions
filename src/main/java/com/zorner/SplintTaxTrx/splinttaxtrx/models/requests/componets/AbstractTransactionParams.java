package com.zorner.SplintTaxTrx.splinttaxtrx.models.requests.componets;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public abstract class AbstractTransactionParams {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("include_reversible")
    private boolean includeReversible;
    
    @Override
    public String toString() {
        return "{ includeReversible=" + this.includeReversible + " }";
    }
}
