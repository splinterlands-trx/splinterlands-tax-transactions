package com.zorner.SplintTaxTrx.splinttaxtrx.models.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum CoinpandaTrxLabel {
    AIRDROP("Airdrop", "Cryptocurrancy given to you and must be claimed before use"),
    FORK("Fork", "Cryptocurrancy gained by a hard fork"),
    MINING("Mining", "Cryptocurrancy gain by providing a service to the blockchain"),
    STAKING("Staking", "Cryptocurrancy gain by having staked Cryptocurrancy"),
    INCOME("Income", "Cryptocurrancy income"),
    GIFT("Gift", "Cryptocurrancy given to you or by you"),
    REALIZED_PL("Realized_P&L", "Cryptocurrancy P&L gain or loss"),
    LOST("Lost", "Lost cryptocurrancy"),
    DONATION("Donation", "Cryptocurrancy donated"),
    COST("Cost", "Cryptocurrancy costs and fee (not transaction fees)"),
    NFT("NFT", "NFT trades"),
    NO_LABEL("", "no label");
    private static final Map<String, CoinpandaTrxLabel> BY_LABEL = new HashMap<>();
    private static final Map<String, CoinpandaTrxLabel> BY_DESCRIPTION = new HashMap<>();
    
    static {
        for (CoinpandaTrxLabel element : values()) {
            BY_LABEL.put(element.label, element);
            BY_DESCRIPTION.put(element.description, element);
        }
    }
    
    private final String label;
    private final String description;
    
    CoinpandaTrxLabel(String label, String description) {
        this.label = label;
        this.description = description;
    }
    
    public static CoinpandaTrxLabel valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }
    
    public static CoinpandaTrxLabel valueOfDESCRIPTION(String description) {
        return BY_DESCRIPTION.get(description);
    }
    
    @Override
    public String toString() {
        return this.label;
    }
    
    @JsonValue
    public String getLabel() {
        return this.label;
    }
    
    public String getDescription() {
        return this.description;
    }
}
