package com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class AddWallet extends AbstractSplint {
    private String address;
}
