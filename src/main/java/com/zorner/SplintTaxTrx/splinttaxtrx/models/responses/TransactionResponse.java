package com.zorner.SplintTaxTrx.splinttaxtrx.models.responses;

import com.zorner.SplintTaxTrx.splinttaxtrx.models.responses.componets.TransactionResult;
import lombok.Getter;

@Getter
public class TransactionResponse extends AbstractResponse {
    private TransactionResult result;
}
