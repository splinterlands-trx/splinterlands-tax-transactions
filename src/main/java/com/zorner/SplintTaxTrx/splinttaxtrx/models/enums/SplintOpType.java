package com.zorner.SplintTaxTrx.splinttaxtrx.models.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum SplintOpType {
    ACCEPT_CHALLENGE("sm_accept_challenge", "6256efa2621ef3cc88bf57ed24b1704fa2e01170"),
    ADD_WALLET("sm_add_wallet", "91061bb66f59bef1f1cb310faddbc27b0dbdb9d4"),
    ADVANCE_LEAGUE("sm_advance_league", "fdc09729783978e717241ff4d83dd3883f0bc100"),
    BURN_CARDS("sm_burn_cards", "f2c1a84b32802b54e2de9dfe9a0061fb179eb08c"),
    CANCEL_MATCH("sm_cancel_match", "65534b7bd626e1d84ce136fe994c468b57333fca"),
    CANCEL_SELL("sm_cancel_sell", "7801128610dbb2ab37fd164803ba82827dd75cb8"),
    CANCEL_UNSTAKE_TOKENS("sm_cancel_unstake_tokens", "95618f381f13d67d5f59b5da7ec221345c088a41"),
    CLAIM_AIRDROP("sm_claim_airdrop", "01fc2a66b25f2ff40e23fae5cb1e69959475abf7"),
    CLAIM_REWARD("sm_claim_reward", "f8c55d6e35c6f266aafb8547f459992136215df2"),
    COMBINE_ALL("sm_combine_all", "4f8f54a252cf5876491568401ac402e43d27a4fc"),
    COMBINE_CARDS("sm_combine_cards", "5fbe2d3b414c0c1bde0c6dd045fc4d98444164e5"),
    CREATE_GUILD("sm_create_guild", "0d36c221bcb49710edd5cd2c1568830a1f091123"),
    DECLINE_CHALLENGE("sm_decline_challenge", "4cd7013edc2c62cc96ae956d85bf194ba7fe0d44"),
    DELEGATE_CARDS("sm_delegate_cards", "6035b4d79dce6b22013e217437c749395b916db3"),
    ENTER_TOURNAMENT("sm_enter_tournament", "4b255e9a96dfff67d79f09857a267d8603e971eb"),
    EXTERNAL_PAYMENT("sm_external_payment", "7562e2247f70b0be45b86b0cda64d9be0dd3f37a"),
    FIND_MATCH("sm_find_match", "798bb76fcd86e5de2e91902d1f50afa7c8dcecdc"),
    GIFT_CARDS("sm_gift_cards", "b4a2b25a8d364f6107e3d9604ad761fde1ad81e7"),
    GIFT_PACKS("sm_gift_packs", "369fb92ca01d54245500cb11210c175db7047465"),
    GUILD_ACCEPT("sm_guild_accept", "b44728c9ea5c44c8b55a0a003db8458549c64591"),
    GUILD_CONTRIBUTION("sm_guild_contribution", "8dd259125f74f0d65320b74aa1f1abc94998c341"),
    GUILD_DECLINE("sm_guild_decline", "4b39fdb430f2fb73f42e79f6b4ac94617b0041c1"),
    GUILD_INVITE("sm_guild_invite", "23a5008c57a57b31d281a143b6afcb7d865ee6e2"),
    GUILD_PROMOTE("sm_guild_promote", "44da6dae5dcfa3a8a1bb188fe4b178a0bd2fa66d"),
    GUILD_PURCHASE("sm_guild_purchase", "3a55ead058195c01a797a633b7de74be5b0da1c7"),
    GUILD_REMOVE("sm_guild_remove", "d05d46a26b4cc701a3ace83a4f3bbadbe2965a86"),
    JOIN_GUILD("sm_join_guild", "7b71fa06d889925423b537a549e8801777b3cb55"),
    LEAVE_GUILD("sm_leave_guild", "f87a4bbab27b74862130cd217076e742155b3009"),
    LEAVE_TOURNAMENT("sm_leave_tournament", "513011ad604ac8ac96043187b17f044ef61dbf83"),
    LOCK_ASSETS("sm_lock_assets", "539ee470ba84594f4b405b4b432e63350c6aac82"),
    MARKET_CANCEL_RENTAL("sm_market_cancel_rental", "dadd1b8b198de07fca9cf8af3c09422575aa7519"),
    MARKET_LIST("sm_market_list", "871935f03bffcd1ccfb1e3baaeca941bea114b46"),
    MARKET_PURCHASE("sm_market_purchase", "72572beb12bd9564a69b7f33b21e7252ed7af00f"),
    MARKET_RENEW_RENTAL("sm_market_renew_rental", "96a663f81adae5289939905ebbd01b3d8a82aa67"),
    MARKET_RENT("sm_market_rent", "cebf830caa56810b5043129e1f82d3bf84e63ad1"),
    MH_DEV_START_QUEST("mh-dev-sm_start_quest", "a8855b9585330cc2a59f550bc004e53f38254780"),
    OPEN_ALL("sm_open_all", "25c236d43c112a288eb33fe975f29e51963eee1b"),
    OPEN_PACK("sm_open_pack", "eec4e4cc7d1b6a956ea45b8e1b2d51e958c65b00"),
    PRICE_FEED("sm_price_feed", "9d3093becf1675fd534d0003a5e078708a2894f2"),
    PURCHASE("sm_purchase", "6ef516f462bd0f6dd66a66992a0448993bb87b62"),
    QA_START_QUEST("qa-sm_start_quest", "e205ebace54f3eefeacb94641e4d5de121f96387"),
    REFRESH_QUEST("sm_refresh_quest", "194f453fd2959852de3928b2248924bc1d781562"),
    SELL_CARDS("sm_sell_cards", "2de36bacca73ad46a67e0bca33ed037313a0b0ad"),
    SET_AUTHORITY("sm_set_authority", "434ab6dbc27ce32ac99d1f62902d9003ba68eaf2"),
    STAKE_TOKENS("sm_stake_tokens", "7d64433d79ebe437ed73b7baf4ea5503e1a6917b"),
    START_MATCH("sm_start_match", "0b8965ea43fae393d1e0e4db0c5dca108800e868"),
    START_QUEST("sm_start_quest", "14b75a8c9f51ee4b0a8501ab96837d6569dc35c5"),
    SUBMIT_TEAM("sm_submit_team", "1a25ba60c71af3b81cf210887abd7ddb16c3b388"),
    SURRENDER("sm_surrender", "52b39fd3b862867b9bdfc024b63ed6c5c2f9396d"),
    TEAM_REVEAL("sm_team_reveal", "d88b52dc0b536b6eb03722f561c98e6b41d0fc40"),
    TOKEN_AWARD("sm_token_award", "b10fd0391ccb08c0760521a4696e755a94a2fdfb"),
    TOKEN_TRANSFER("sm_token_transfer", "06920c5d7ce3f5a6b4aa3c59822e5c280c8a8b04"),
    TOURNAMENT_CHECK_IN("sm_tournament_checkin", "2e2db5d359cac3dc3b8cad2576ccf71955645605"),
    UNDELEGATE_CARDS("sm_undelegate_cards", "629174e504fee9529906850b4a64c4c3f8e30b00"),
    UNLOCK_ASSETS("sm_unlock_assets", "93adcb506603c1856724d3de6f562dc24204c663"),
    UNSTAKE_TOKENS("sm_unstake_tokens", "a2f7b15dd411ea8636d7d947b666d92d6fe4f98a"),
    UPDATE_PRICE("sm_update_price", "588b6efed84baa789cffcef13355fc4a19669c4f"),
    UPDATE_RENTAL_PRICE("sm_update_rental_price", "2d0a56fee6e6a60d8b258dd184d4969005d39688"),
    UPGRADE_ACCOUNT("sm_upgrade_account", "d05728e6916d8ba6d8c1f85179f05d9b216d8eb6"),
    WORDEMPIRE_DEV_BURN_CARDS("wordempire-dev-sm_burn_cards", "281c72203e24b3c4a08c7efebe2fdd2615543ae6");
    
    private final String label;
    private final String refTrx;
    private static final Map<String, SplintOpType> BY_LABEL = new HashMap<>();
    private static final Map<String, SplintOpType> BY_REFTRX = new HashMap<>();
    
    static {
        for (SplintOpType element : values()) {
            BY_LABEL.put(element.label, element);
            BY_REFTRX.put(element.refTrx, element);
        }
    }
    
    SplintOpType(String label, String refTrx) {
        this.label = label;
        this.refTrx = refTrx;
    }
    
    public static SplintOpType valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }
    
    public static SplintOpType valueOfREFTRX(String refTrx) {
        return BY_REFTRX.get(refTrx);
    }
    
    @Override
    public String toString() {
        return this.label;
    }
    
    @JsonValue
    public String getLabel() {
        return this.label;
    }
    
    public String getRefTrx() {
        return this.refTrx;
    }
}
