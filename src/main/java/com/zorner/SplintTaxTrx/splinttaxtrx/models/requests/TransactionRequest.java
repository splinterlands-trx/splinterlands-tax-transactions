package com.zorner.SplintTaxTrx.splinttaxtrx.models.requests;

import com.zorner.SplintTaxTrx.splinttaxtrx.models.requests.componets.TransactionParams;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
//@NoArgsConstructor
public class TransactionRequest extends AbstractRequest {
    private TransactionParams params;
    
    public TransactionRequest() {
        this.setMethod("account_history_api.get_transaction"); // TODO: Move var here?
    }
    
    @Override
    public String toString() {
        return "{ jsonrpc='" + this.getJsonrpc() + ", params=" + this.getParams() + "', id=" + this.getId() + ", method='" + this.getMethod() + "' }";
    }
}
