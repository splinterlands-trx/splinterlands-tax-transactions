package com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zorner.SplintTaxTrx.splinttaxtrx.interfaces.SplintTrxInerface;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.Transaction;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.CryptoTransaction;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint.componets.Pack;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint.componets.PriceCurrency;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint.componets.SplintItem;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.enums.CoinpandaTrxLabel;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.enums.CoinpandaTrxType;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.requests.TransactionRequest;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.requests.componets.TransactionParams;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.responses.TransactionResponse;
import com.zorner.SplintTaxTrx.splinttaxtrx.services.SplintRequests;
import com.zorner.SplintTaxTrx.splinttaxtrx.transformers.HiveTrxTransformer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Getter
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class MarketPurchase extends AbstractSplint implements SplintTrxInerface {
    
    @Autowired
    @JsonIgnore
    HiveTrxTransformer hiveTrxTransformer = new HiveTrxTransformer();
    
    @Autowired
    @JsonIgnore
    SplintRequests splintRequests = new SplintRequests();
    
    private List<SplintItem> items;
    private BigDecimal price;
    private String currency;
    private String market;
    
    @Override
    public List<CryptoTransaction> getCryptoDetail(CryptoTransaction transaction) {
        List<CryptoTransaction> trxes = new ArrayList<>();
        List<PriceCurrency> priceCurrencies = new ArrayList<>();
        BigDecimal sum = new BigDecimal(0);
        for (SplintItem item : items) {
            CryptoTransaction trx = new CryptoTransaction(transaction);
            TransactionResponse response = splintRequests.getTransaction(new TransactionRequest().toBuilder()
                    .params(TransactionParams.builder()
                            .id(item.getTrxHash())
                            .build())
                    .build());
            Transaction<AbstractSplint> transformedTransaction = hiveTrxTransformer.getTransaction(response);
            if (transformedTransaction.getDetails() instanceof SellCards) {
                Pack cardPack = ((SellCards) transformedTransaction.getDetails()).getPacks().get(item.getIndex());
                if (cardPack.getCurrency().equals("USD")) {
                    for (String card : cardPack.getCards()) {
                        trx.setType(CoinpandaTrxType.TRADE);
                        trx.setSentCurrency(cardPack.getCurrency());
                        trx.setReceivedAmount(BigDecimal.valueOf(1));
                        trx.setReceivedCurrency(card);
                        trx.setDescription(card);
                        trx.setLabel(CoinpandaTrxLabel.NFT);
                        if (cardPack.getPrice().doubleValue() > 0) {
                            priceCurrencies.add(PriceCurrency.builder()
                                    .value(cardPack.getPrice().divide(BigDecimal.valueOf(cardPack.getCards().size()), 7, RoundingMode.HALF_UP)) //TODO find out the right setting
                                    .currency(cardPack.getCurrency())
                                    .build());
                            sum = sum.add(cardPack.getPrice().divide(BigDecimal.valueOf(cardPack.getCards().size()), 7, RoundingMode.HALF_UP)); //TODO find out the right setting
                            trxes.add(trx);
                        } else {
                            if (cardPack.getPrice().doubleValue() != 0)
                                log.warn("Negative price in transaction: " + item.getTrxHash() + ". Not adding transaction.");
                            else
                                trxes.add(trx);
                        }
                    }
                } else {
                    log.warn(item.getTrxHash() + " is not using USD currency as expected. Skipping this transaction.");
                }
            } else {
                log.warn(item.getTrxHash() + " is not a sm_sell_card transaction as expected. Skipping this transaction.");
            }
        }
        for (int index = 0; index < trxes.size(); index++) {
            if (sum.doubleValue() > 0)
                trxes.get(index).setSentAmount(this.price.multiply(priceCurrencies.get(index).getValue().divide(sum, 7, RoundingMode.HALF_UP))); //TODO find out the right setting
        }
        return trxes;
    }
}
