package com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zorner.SplintTaxTrx.splinttaxtrx.interfaces.SplintTrxInerface;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.CryptoTransaction;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint.componets.CombinedFinal;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.enums.CoinpandaTrxLabel;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.enums.CoinpandaTrxType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Slf4j
@Getter
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class CombineCards extends AbstractSplint implements SplintTrxInerface {
    @JsonIgnore
    private static final List<CombinedFinal> finalTrxes = new ArrayList<>();
    private List<String> cards;
    
    public static List<CryptoTransaction> getCombinedTransaction(List<CryptoTransaction> history) {
        List<CryptoTransaction> trxes = new ArrayList<>();
        Collections.sort(finalTrxes);
        for (CombinedFinal combinedFinal : finalTrxes) {
            BigDecimal sum = BigDecimal.valueOf(0);
            for (String card : combinedFinal.getCards()) {
                Optional<CryptoTransaction> last = history.stream()
                        .filter(trx -> combinedFinal.getTrx().getDate().compareTo(trx.getDate()) >= 0 &&
                                card.equals(trx.getReceivedCurrency()))
                        .max(Comparator.comparing(CryptoTransaction::getDate));
                if (last.isPresent() && combinedFinal.getTrx().getSentCurrency().equals(last.get().getSentCurrency())) {
                    sum = sum.add(last.get().getSentAmount());
                    history.remove(last.get());
                } else {
                    if (last.isPresent()) {
                        log.warn("Purchase found, but sent currency not the same as need for re-buy of combined card. Manual intervention required. Sent Currency" +
                                combinedFinal.getTrx().getSentCurrency() + "\nFound transaction: " + last.get());
                        System.out.println("Purchase found, but sent currency not the same as need for re-buy of combined card. Manual intervention required. Sent Currency" +
                                combinedFinal.getTrx().getSentCurrency() + "\nFound transaction: " + last.get());
                    } else {
                        log.warn("No purchase of " + card + " found for \ntransaction: " + combinedFinal.getTrx());
                        System.out.println("No purchase of " + card + " found for \ntransaction: " + combinedFinal.getTrx());
                    }
                }
            }
            combinedFinal.getTrx().setSentAmount(sum);
            history.add(combinedFinal.getTrx());
            trxes.add(combinedFinal.getTrx());
        }
        return trxes;
    }
    
    @Override
    public List<CryptoTransaction> getCryptoDetail(CryptoTransaction transaction) {
        
        List<CryptoTransaction> trxes = new ArrayList<>();
        CryptoTransaction reverseTrx = new CryptoTransaction(transaction);
        reverseTrx.setType(CoinpandaTrxType.TRADE);
        reverseTrx.setSentCurrency("DEC");
        reverseTrx.setReceivedAmount(BigDecimal.valueOf(1));
        reverseTrx.setReceivedCurrency(cards.get(0));
        reverseTrx.setLabel(CoinpandaTrxLabel.NFT);
        reverseTrx.setDescription("Combine purchase of " + cards.get(0));
        finalTrxes.add(CombinedFinal.builder()
                .trx(reverseTrx)
                .cards(this.cards)
                .build());
        for (String card : this.cards) {
            CryptoTransaction trx = new CryptoTransaction(transaction);
            trx.setType(CoinpandaTrxType.TRADE);
            trx.setSentAmount(BigDecimal.valueOf(1));
            trx.setSentCurrency(card);
            trx.setReceivedAmount(BigDecimal.valueOf(0));
            trx.setReceivedCurrency("DEC");
            trx.setLabel(CoinpandaTrxLabel.NFT);
            trx.setDescription("Combine sale of " + card);
            trxes.add(trx);
        }
        return trxes;
    }
}
