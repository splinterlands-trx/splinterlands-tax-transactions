package com.zorner.SplintTaxTrx.splinttaxtrx.models.requests.componets;

import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class TransactionParams extends AbstractTransactionParams {
    @NonNull
    private String id;
    
    @Override
    public String toString() {
        return "{ id='" + this.id + "', includeReversible=" + this.isIncludeReversible() + " }";
    }
}
