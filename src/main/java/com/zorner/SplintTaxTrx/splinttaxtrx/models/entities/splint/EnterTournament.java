package com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.zorner.SplintTaxTrx.splinttaxtrx.interfaces.SplintTrxInerface;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.CryptoTransaction;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;

@Getter
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class EnterTournament extends AbstractSplint implements SplintTrxInerface {
    @JsonProperty("tournament_id")
    private String tournamentId;
    @JsonProperty("signed_pw")
    private String signedPassword;
    
    @Override
    public List<CryptoTransaction> getCryptoDetail(CryptoTransaction trx) {
        List<CryptoTransaction> trxes = new ArrayList<>();  // TODO find how to determine entry fee
        return trxes;
    }
}
