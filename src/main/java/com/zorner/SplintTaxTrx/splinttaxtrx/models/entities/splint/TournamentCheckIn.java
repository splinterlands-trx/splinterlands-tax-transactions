package com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class TournamentCheckIn extends AbstractSplint {
    @JsonProperty("tournament_id")
    private String tournamentId;
}
