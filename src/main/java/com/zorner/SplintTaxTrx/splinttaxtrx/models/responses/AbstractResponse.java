package com.zorner.SplintTaxTrx.splinttaxtrx.models.responses;

import lombok.Getter;

@Getter
public abstract class AbstractResponse {
    private String jsonrpc;
    private int id;
}