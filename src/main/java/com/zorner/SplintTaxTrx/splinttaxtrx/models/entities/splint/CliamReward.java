package com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.zorner.SplintTaxTrx.splinttaxtrx.interfaces.SplintTrxInerface;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.CryptoTransaction;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;

@Getter
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class CliamReward extends AbstractSplint implements SplintTrxInerface {
    private long season;
    @JsonProperty("quest_id")
    private String questId;
    
    @Override
    public List<CryptoTransaction> getCryptoDetail(CryptoTransaction trx) {
        List<CryptoTransaction> trxes = new ArrayList<>(); // TODO find how to get to reward
        return trxes;
    }
}
