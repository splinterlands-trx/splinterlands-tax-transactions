package com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.zorner.SplintTaxTrx.splinttaxtrx.interfaces.SplintTrxInerface;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.Transaction;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.CryptoTransaction;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint.componets.SplintItem;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.enums.CoinpandaTrxLabel;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.enums.CoinpandaTrxType;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.requests.TransactionRequest;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.requests.componets.TransactionParams;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.responses.TransactionResponse;
import com.zorner.SplintTaxTrx.splinttaxtrx.services.SplintRequests;
import com.zorner.SplintTaxTrx.splinttaxtrx.transformers.HiveTrxTransformer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Getter
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class MarketRent extends AbstractSplint implements SplintTrxInerface {
    
    @Autowired
    @JsonIgnore
    HiveTrxTransformer hiveTrxTransformer = new HiveTrxTransformer();
    
    @Autowired
    @JsonIgnore
    SplintRequests splintRequests = new SplintRequests();
    
    private List<SplintItem> items;
    private String currency;
    @JsonProperty("limit_price")
    private BigDecimal limitPrice;
    private String market;
    private int days;
    
    @Override
    public List<CryptoTransaction> getCryptoDetail(CryptoTransaction transaction) {
        List<CryptoTransaction> trxes = new ArrayList<>();
        for (SplintItem item : items) {
            CryptoTransaction trx = new CryptoTransaction(transaction);
            TransactionResponse response = splintRequests.getTransaction(new TransactionRequest().toBuilder()
                    .params(TransactionParams.builder()
                            .id(item.getTrxHash())
                            .build())
                    .build());
            Transaction<AbstractSplint> transformedTransaction = hiveTrxTransformer.getTransaction(response);
            if (transformedTransaction.getDetails() instanceof MarketList) {
                MarketList marketList = (MarketList) transformedTransaction.getDetails();
                if (marketList.getType().equals("rent")) {
                    trx.setType(CoinpandaTrxType.SEND);
                    trx.setSentAmount(marketList.getCards().get(item.getIndex()).getCost().multiply(BigDecimal.valueOf(this.days)));
                    trx.setSentCurrency(this.currency);
                    trx.setLabel(CoinpandaTrxLabel.COST);
                    trx.setDescription(trx.getSplintType().toString() + " of " + marketList.getCards().get(item.getIndex()).getCard());
                    trxes.add(trx);
                } else {
                    log.warn(item.getTrxHash() + " transaction type is not 'rent' as expected. Skipping this transaction.");
                }
            } else {
                log.warn(item.getTrxHash() + " is not a sm_market_list transaction as expected. Skipping this transaction.");
            }
        }
        return trxes;
    }
}
