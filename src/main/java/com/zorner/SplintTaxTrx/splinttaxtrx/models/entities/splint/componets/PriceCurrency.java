package com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint.componets;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;

@Builder
@Getter
public class PriceCurrency {
    private BigDecimal value;
    private String currency;
}