package com.zorner.SplintTaxTrx.splinttaxtrx.models.responses.componets;

import lombok.Getter;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Getter
public class HistoryListResponse {
    private List<HistoryMapEntryResponse> history;
    
    public Map<Integer, HistoryTrxResponse> getTrxMap() {
        Map<Integer, HistoryTrxResponse> trxMap = new TreeMap<>();
        for (HistoryMapEntryResponse mapEntry : this.history) {
            trxMap.put(mapEntry.getKey(), mapEntry.getTrx());
        }
        return trxMap;
    }
}
