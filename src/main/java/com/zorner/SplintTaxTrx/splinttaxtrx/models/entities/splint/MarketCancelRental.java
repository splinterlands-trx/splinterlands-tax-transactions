package com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint;

import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint.componets.SplintItem;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class MarketCancelRental extends AbstractSplint {
    private List<SplintItem> items;
}
