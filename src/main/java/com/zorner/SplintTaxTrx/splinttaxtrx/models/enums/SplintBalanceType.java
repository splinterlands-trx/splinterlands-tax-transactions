package com.zorner.SplintTaxTrx.splinttaxtrx.models.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum SplintBalanceType {
    CLAIM_STAKING_REWARDS("claim_staking_rewards", CoinpandaTrxType.RECEIVE, CoinpandaTrxLabel.STAKING, SplintOpType.CLAIM_REWARD),
    DEC_REWARD("dec_reward", CoinpandaTrxType.RECEIVE, CoinpandaTrxLabel.MINING, SplintOpType.CLAIM_REWARD),
    ENTER_TOURNAMENT("enter_tournament", CoinpandaTrxType.SEND, CoinpandaTrxLabel.COST, SplintOpType.ENTER_TOURNAMENT),
    MARKET_FEES("market_fees", CoinpandaTrxType.SEND, CoinpandaTrxLabel.COST, SplintOpType.MARKET_LIST),
    MARKET_PURCHASE("market_purchase", CoinpandaTrxType.TRADE, CoinpandaTrxLabel.NFT, SplintOpType.MARKET_PURCHASE),
    MARKET_RENTAL("market_rental", CoinpandaTrxType.SEND, CoinpandaTrxLabel.COST, SplintOpType.MARKET_RENT),
    QUEST_REWARDS("quest_rewards", CoinpandaTrxType.RECEIVE, CoinpandaTrxLabel.MINING, SplintOpType.CLAIM_REWARD),
    RENTAL_PAYMENT("rental_payment", CoinpandaTrxType.RECEIVE, CoinpandaTrxLabel.INCOME, SplintOpType.EXTERNAL_PAYMENT),
    RENTAL_PAYMENT_FEES("rental_payment_fees", CoinpandaTrxType.SEND, CoinpandaTrxLabel.COST, SplintOpType.MARKET_LIST),
    SEASON_REWARDS("season_rewards", CoinpandaTrxType.RECEIVE, CoinpandaTrxLabel.MINING, SplintOpType.CLAIM_REWARD),
    STAKE_TOKENS("stake_tokens", CoinpandaTrxType.SEND, CoinpandaTrxLabel.COST, SplintOpType.STAKE_TOKENS),
    TOKEN_AWARD("token_award", CoinpandaTrxType.RECEIVE, CoinpandaTrxLabel.AIRDROP, SplintOpType.TOKEN_AWARD),
    TOKEN_TRANSFER("token_transfer", CoinpandaTrxType.TRANSFER, CoinpandaTrxLabel.NO_LABEL, SplintOpType.TOKEN_TRANSFER),
    VOUCHER_DROP("voucher_drop", CoinpandaTrxType.RECEIVE, CoinpandaTrxLabel.STAKING, SplintOpType.TOKEN_AWARD);
    
    private static final Map<String, SplintBalanceType> BY_LABEL = new HashMap<>();
    
    static {
        for (SplintBalanceType element : values()) {
            BY_LABEL.put(element.label, element);
        }
    }
    
    private final String label;
    private final CoinpandaTrxType type;
    private final CoinpandaTrxLabel cpLabel;
    private final SplintOpType opType;
    
    SplintBalanceType(String label, CoinpandaTrxType type, CoinpandaTrxLabel cpLabel, SplintOpType opType) {
        this.label = label;
        this.type = type;
        this.cpLabel = cpLabel;
        this.opType = opType;
    }
    
    public static SplintBalanceType valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }
    
    @Override
    public String toString() {
        return this.label;
    }
    
    @JsonValue
    public String getLabel() {
        return this.label;
    }
    
    public CoinpandaTrxType getType() {
        return type;
    }
    
    public CoinpandaTrxLabel getCoinpandaLabel() {
        return this.cpLabel;
    }
    
    public SplintOpType getSplintType() {
        return this.opType;
    }
}
