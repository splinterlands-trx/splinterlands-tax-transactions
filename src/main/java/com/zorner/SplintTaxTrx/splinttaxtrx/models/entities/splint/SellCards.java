package com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint;

import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint.componets.Pack;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@Getter
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class SellCards extends AbstractSplint {
    private List<Pack> packs;

//    @Override
//    public List<CryptoTransaction> getCryptoDetail(CryptoTransaction trx) {
//        List<CryptoTransaction> trxes = new ArrayList<>();
//        for(Pack pack : this.packs) {
//            if (pack.getCurrency().equals("USD")) {
//                for (String card : pack.getCards()) {
//                    trx.setType(CoinpandaTrxType.TRADE);
//                    trx.setSentAmount(BigDecimal.valueOf(1));
//                    trx.setSentCurrency(card);
//                    trx.setReceivedCurrency(pack.getCurrency()); // TODO Needs to be converted to DEC
//                    trx.setFeeCurrency(pack.getCurrency()); // TODO Needs to be converted to DEC
//                    trx.setLabel(CoinpandaTrxLabel.NFT);
//                    trx.setDescription(card);
//                    BigDecimal fee = pack.getPrice().multiply(BigDecimal.valueOf(pack.getFee_pct()).multiply(BigDecimal.valueOf(0.0001)));
//                    if (pack.getPrice().doubleValue() > 0) {
//                        trx.setReceivedAmount(pack.getPrice()
//                                .divide(BigDecimal.valueOf(pack.getCards().size()), 7, RoundingMode.HALF_UP));  // TODO Needs to be converted to DEC //TODO find out the right setting
//                        trx.setFeeAmount(fee
//                                .divide(BigDecimal.valueOf(pack.getCards().size()), 7, RoundingMode.HALF_UP));  // TODO Needs to be converted to DEC //TODO find out the right setting
//                        trxes.add(trx);
//                    } else {
//                        if (pack.getPrice().doubleValue() != 0)
//                            log.warn("Negative price in transaction: " + trx.getTrxHash() + ". Not adding transaction.");
//                        else
//                            trxes.add(trx);
//                    }
//                }
//            } else {
//                log.warn(trx.getTrxHash() + " is not a sm_sell_card transaction as expected. Skipping this transaction.");
//            }
//        }
//        return trxes;
//    }
}
