package com.zorner.SplintTaxTrx.splinttaxtrx.models.responses.componets;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.util.List;

@Getter
public class ValueResponse {
    @JsonProperty("required_auths")
    private List<String> requiredAuths;
    @JsonProperty("required_posting_auths")
    private List<String> requiredPostingAuths;
    private String id;
    private String json;
}
