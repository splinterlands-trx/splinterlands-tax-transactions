package com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint;

import com.zorner.SplintTaxTrx.splinttaxtrx.interfaces.SplintTrxInerface;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.CryptoTransaction;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.enums.CoinpandaTrxLabel;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.enums.CoinpandaTrxType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class StakeTokens extends AbstractSplint implements SplintTrxInerface {
    private String token;
    private BigDecimal qty;
    
    @Override
    public List<CryptoTransaction> getCryptoDetail(CryptoTransaction trx) {
        List<CryptoTransaction> trxes = new ArrayList<>();
        trx.setType(CoinpandaTrxType.SEND);
        trx.setSentAmount(this.qty);
        trx.setSentCurrency(this.token);
        trx.setLabel(CoinpandaTrxLabel.COST);
        trx.setDescription(trx.getSplintType().toString());
        trxes.add(trx);
        return trxes;
    }
}
