package com.zorner.SplintTaxTrx.splinttaxtrx.models.responses;

import com.zorner.SplintTaxTrx.splinttaxtrx.models.responses.componets.HistoryListResponse;
import lombok.Getter;

@Getter
public class HistoryResponse extends AbstractResponse {
    private HistoryListResponse result;
}
