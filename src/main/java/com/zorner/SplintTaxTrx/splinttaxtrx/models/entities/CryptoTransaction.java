package com.zorner.SplintTaxTrx.splinttaxtrx.models.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.zorner.SplintTaxTrx.splinttaxtrx.interfaces.ConfirmTransaction;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.enums.CoinpandaTrxLabel;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.enums.CoinpandaTrxType;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.enums.SplintOpType;
import com.zorner.SplintTaxTrx.splinttaxtrx.utils.BigDecimalSerializer;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.PositiveOrZero;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.regex.Pattern;

@Entity
@Table(name = "CryptoTransaction")
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor//(access = AccessLevel.PACKAGE) // TODO fix access levels
@Setter//(value = AccessLevel.PACKAGE)
@Getter
@JsonPropertyOrder({"Date", "Type", "Sent Amount", "Sent Curr", "Received Amount", "Received Curr",
        "Fee Amount", "Fee Curr", "Label", "Description", "TxHash"})
@ConfirmTransaction
public class CryptoTransaction implements Serializable {
    private static final Pattern pattern = Pattern.compile("^[A-Z]+$");
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Setter(AccessLevel.PROTECTED)
    @JsonIgnore
    private Long id;
    //    @NotBlank(message = "date may not be empty")  // Causes JPA to fail without info in logs as to why
    @Column(nullable = false)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("Date")
    private LocalDateTime date;
    @JsonProperty("Type")
    private CoinpandaTrxType type;
    @PositiveOrZero
    @Column(scale = 7, precision = 16) //TODO find out the right setting
    @JsonProperty("Sent Amount")
    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal sentAmount;
    @JsonProperty("Sent Curr")
    private String sentCurrency;
    @PositiveOrZero
    @Column(scale = 7, precision = 16) //TODO find out the right setting
    @JsonProperty("Received Amount")
    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal receivedAmount;
    @JsonProperty("Received Curr")
    private String receivedCurrency;
    @PositiveOrZero
    @Column(scale = 7, precision = 16) //TODO find out the right setting
    @JsonProperty("Fee Amount")
    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal feeAmount;
    @JsonProperty("Fee Curr")
    private String feeCurrency;
    @JsonProperty("Label")
    private CoinpandaTrxLabel label;
    @JsonProperty("Description")
    private String description;
    @JsonProperty("TxHash")
    private String trxHash;
    @JsonIgnore
    private SplintOpType splintType;
    @CreationTimestamp
    @JsonIgnore
    private Date createdAt;
    @UpdateTimestamp
    @JsonIgnore
    private Date updatedAt;
    
    @SuppressWarnings("CopyConstructorMissesField")
    public CryptoTransaction(CryptoTransaction trx) {
        this.date = trx.date;
        this.type = trx.type;
        this.sentAmount = trx.sentAmount;
        this.sentCurrency = trx.sentCurrency;
        this.receivedAmount = trx.receivedAmount;
        this.receivedCurrency = trx.receivedCurrency;
        this.feeAmount = trx.feeAmount;
        this.feeCurrency = trx.feeCurrency;
        this.label = trx.label;
        this.description = trx.description;
        this.trxHash = trx.trxHash;
        this.splintType = trx.splintType;
    }
    
    public boolean isSimilar(CryptoTransaction trx) {
        long allowance = 10L;
        boolean nftMatch = true;
        if (this.label == CoinpandaTrxLabel.NFT) {
            if (this.sentAmount != null && pattern.matcher(this.sentCurrency).matches() && this.sentCurrency.equals(trx.sentCurrency)) {
                nftMatch = trx.sentAmount.compareTo(this.sentAmount.subtract(BigDecimal.valueOf(0.002))) > 0 &&
                        trx.sentAmount.compareTo(this.sentAmount.add(BigDecimal.valueOf(0.002))) < 0;
            }
            if (this.receivedAmount != null && pattern.matcher(this.receivedCurrency).matches() && this.receivedCurrency.equals(trx.receivedCurrency)) {
                nftMatch = nftMatch && trx.receivedAmount.compareTo(this.receivedAmount.subtract(BigDecimal.valueOf(0.002))) > 0 &&
                        trx.receivedAmount.compareTo(this.receivedAmount.add(BigDecimal.valueOf(0.002))) < 0;
            }
            if (this.splintType == SplintOpType.COMBINE_CARDS) {
                nftMatch = nftMatch && this.sentCurrency.equals(trx.sentCurrency) && this.receivedCurrency.equals(trx.receivedCurrency);
            }
        }
        return this.splintType.equals(trx.splintType) && nftMatch && (this.date.isEqual(trx.date) ||
                (this.date.minusSeconds(allowance).isBefore(trx.date) && this.date.plusSeconds(allowance).isAfter(trx.date)));
    }
    
    public boolean areAmountsEqual(CryptoTransaction trx) {
        boolean nftMatch = true;
        if (this.label == CoinpandaTrxLabel.NFT) {
            if (this.sentCurrency != null && pattern.matcher(this.sentCurrency).matches()) {
                nftMatch = (this.sentCurrency != null && this.sentCurrency.equals(trx.sentCurrency)) ||
                        (this.sentCurrency == null && trx.sentCurrency == null);
            }
            if (this.receivedCurrency != null && pattern.matcher(this.receivedCurrency).matches()) {
                nftMatch = nftMatch && (this.receivedCurrency != null && this.receivedCurrency.equals(trx.receivedCurrency)) ||
                        (this.receivedCurrency == null && trx.receivedCurrency == null);
            }
        }
        return nftMatch &&
                ((this.sentAmount == null && trx.sentAmount == null) ||
                        (this.sentAmount != null && trx.sentAmount != null &&
                                this.sentAmount.compareTo(trx.sentAmount) == 0)) &&
                ((this.receivedAmount == null && trx.receivedAmount == null) ||
                        (this.receivedAmount != null && trx.receivedAmount != null &&
                                this.receivedAmount.compareTo(trx.receivedAmount) == 0)) &&
                ((this.feeAmount == null && trx.feeAmount == null) ||
                        (this.feeAmount != null && trx.feeAmount != null &&
                                this.feeAmount.compareTo(trx.feeAmount) == 0)) &&
                ((this.feeCurrency == null && trx.getFeeCurrency() == null) ||
                        (this.feeCurrency != null && trx.feeCurrency != null && this.feeCurrency.equals(trx.feeCurrency)));
    }
    
    public boolean hasAmount() {
        return (this.sentAmount == null || this.receivedAmount != null || !(this.sentAmount.compareTo(BigDecimal.valueOf(0L)) == 0)) &&
                (this.receivedAmount == null || this.sentAmount != null || !(this.receivedAmount.compareTo(BigDecimal.valueOf(0L)) == 0)) &&
                (this.feeAmount == null || ((this.sentAmount == null || this.receivedAmount == null) && !(this.feeAmount.compareTo(BigDecimal.valueOf(0L)) == 0))) &&
                !(this.sentAmount == null && this.receivedAmount == null && this.feeAmount == null);
    }
    
    @Override
    public String toString() {
        return "{ date=" + this.date + ", type=" + this.type +
                ", sentAmount=" + this.sentAmount + ", sentCurrency=" + this.sentCurrency +
                ", receivedAmount=" + this.receivedAmount + ", receivedCurrency=" + this.receivedCurrency +
                ", feeAmount=" + this.feeAmount + ", feeCurrency=" + this.feeCurrency +
                ", label=" + this.label + ", description=" + this.description + ", trxHash=" + this.trxHash + " }";
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        
        if (!(obj instanceof CryptoTransaction))
            return false;
        
        CryptoTransaction trx = (CryptoTransaction) obj;
        
        return this.date != null && this.date.equals(trx.date) &&
                ((this.sentAmount != null && trx.sentAmount != null && this.sentAmount.compareTo(trx.sentAmount) == 0) ||
                        (this.sentAmount == null && trx.sentAmount == null)) &&
                ((this.sentCurrency != null && trx.sentCurrency != null && this.sentCurrency.equals(trx.sentCurrency)) ||
                        (this.sentCurrency == null && trx.sentCurrency == null)) &&
                ((this.receivedAmount != null && trx.receivedAmount != null && this.receivedAmount.compareTo(trx.receivedAmount) == 0) ||
                        (this.receivedAmount == null && trx.receivedAmount == null)) &&
                ((this.receivedCurrency != null && trx.receivedCurrency != null && this.receivedCurrency.equals(trx.receivedCurrency)) ||
                        (this.receivedCurrency == null && trx.receivedCurrency == null)) &&
                ((this.feeAmount != null && trx.feeAmount != null && this.feeAmount.compareTo(trx.feeAmount) == 0) ||
                        (this.feeAmount == null && trx.feeAmount == null)) &&
                ((this.feeCurrency != null && trx.feeCurrency != null && this.feeCurrency.equals(trx.feeCurrency)) ||
                        (this.feeCurrency == null && trx.feeCurrency == null));
    }
    
    @Override
    public int hashCode() {
        int hash = 31;
        hash = 83 * hash + this.date.hashCode();
        hash = 83 * hash + (this.sentAmount == null ? 1 : this.sentAmount.hashCode());
        hash = 83 * hash + (this.sentCurrency == null ? 1 : this.sentCurrency.hashCode());
        hash = 83 * hash + (this.receivedAmount == null ? 1 : this.receivedAmount.hashCode());
        hash = 83 * hash + (this.receivedCurrency == null ? 1 : this.receivedCurrency.hashCode());
        hash = 83 * hash + (this.feeAmount == null ? 1 : this.feeAmount.hashCode());
        hash = 83 * hash + (this.feeCurrency == null ? 1 : this.feeCurrency.hashCode());
        return hash;
    }
}