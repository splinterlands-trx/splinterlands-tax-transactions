package com.zorner.SplintTaxTrx.splinttaxtrx.models.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum CoinpandaTrxType {
    BUY("Buy", "Buying cryptocurrency with fiat"),
    SELL("Sell", "Selling cryptocurrency for fiat"),
    TRADE("Trade", "Exchanging any cryptocurrency for another cryptocurrency"),
    TRANSFER("Transfer", "A transfer of any cryptocurrency or fiat between your own accounts or wallets"),
    RECEIVE("Receive", "An increase of your holdings"),
    SEND("Send", "A decrease of your holdings");
    //    WITHDRAW("Withdraw", "A decrease of your holdings to an unknown wallet"),
//    DEPOSIT("Deposit", "An increase of your holdings from an unknown wallet");
    private static final Map<String, CoinpandaTrxType> BY_LABEL = new HashMap<>();
    private static final Map<String, CoinpandaTrxType> BY_DESCRIPTION = new HashMap<>();
    
    static {
        for (CoinpandaTrxType element : values()) {
            BY_LABEL.put(element.label, element);
            BY_DESCRIPTION.put(element.description, element);
        }
    }
    
    private final String label;
    private final String description;
    
    CoinpandaTrxType(String label, String description) {
        this.label = label;
        this.description = description;
    }
    
    public static CoinpandaTrxType valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }
    
    public static CoinpandaTrxType valueOfDESCRIPTION(String description) {
        return BY_DESCRIPTION.get(description);
    }
    
    @Override
    public String toString() {
        return this.label;
    }
    
    @JsonValue
    public String getLabel() {
        return this.label;
    }
    
    public String getDescription() {
        return this.description;
    }
}
