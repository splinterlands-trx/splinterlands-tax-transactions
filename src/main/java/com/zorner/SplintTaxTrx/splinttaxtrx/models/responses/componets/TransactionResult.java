package com.zorner.SplintTaxTrx.splinttaxtrx.models.responses.componets;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
public class TransactionResult {
    @JsonProperty("ref_block_num")
    private long refBlockNum;
    @JsonProperty("ref_block_prefix")
    private long refBlockPrefix;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime expiration;
    private List<OpResponse> operations;
    private List extensions;
    private List<String> signatures;
    @JsonProperty("transaction_id")
    private String transactionId;
    @JsonProperty("block_num")
    private long blockNum;
    @JsonProperty("transaction_num")
    private int transactionNum;
}
