package com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint.componets;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.List;

@Getter
public class Pack {
    private List<String> cards;
    private String currency;
    private BigDecimal price;
    @JsonProperty("fee_pct")
    private int feePercentage;
}
