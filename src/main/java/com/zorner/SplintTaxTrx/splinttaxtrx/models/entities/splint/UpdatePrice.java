package com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint.componets.SplintItem;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.util.List;

@Getter
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class UpdatePrice extends AbstractSplint {
    private List<SplintItem> ids;
    @JsonProperty("new_price")
    private BigDecimal newPrice;
}
