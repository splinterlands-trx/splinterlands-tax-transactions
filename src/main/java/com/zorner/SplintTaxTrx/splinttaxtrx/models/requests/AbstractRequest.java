package com.zorner.SplintTaxTrx.splinttaxtrx.models.requests;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter(value = AccessLevel.PROTECTED)
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
//@Configuration
//@PropertySource("classpath:splintTaxTrx.properties")
public abstract class AbstractRequest {
    //    @Value("${jsonpc}") //TODO get properties to read in
    private final String jsonrpc = "2.0";
    private final int id = 1;
    private String method;
    
    @Override
    public String toString() {
        return "{ jsonrpc='" + this.jsonrpc + "', id=" + this.id + ", method='" + this.method + "' }";
    }
}