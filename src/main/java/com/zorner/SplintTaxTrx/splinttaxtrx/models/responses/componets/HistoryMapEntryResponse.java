package com.zorner.SplintTaxTrx.splinttaxtrx.models.responses.componets;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;

@Getter
@JsonFormat(shape = JsonFormat.Shape.ARRAY)
@JsonPropertyOrder({"key", "trx"})
public class HistoryMapEntryResponse {
    private int key;
    private HistoryTrxResponse trx;
}
