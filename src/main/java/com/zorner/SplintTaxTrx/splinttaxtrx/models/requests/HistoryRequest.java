package com.zorner.SplintTaxTrx.splinttaxtrx.models.requests;

import com.zorner.SplintTaxTrx.splinttaxtrx.models.requests.componets.HistoryParams;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
//@NoArgsConstructor
public class HistoryRequest extends AbstractRequest {
    private HistoryParams params;
    
    public HistoryRequest() {
        this.setMethod("account_history_api.get_account_history");
    }  // TODO: Move var here?
    
    @Override
    public String toString() {
        return "{ jsonrpc='" + this.getJsonrpc() + ", params=" + this.getParams() + "', id=" + this.getId() + ", method='" + this.getMethod() + "' }";
    }
}
