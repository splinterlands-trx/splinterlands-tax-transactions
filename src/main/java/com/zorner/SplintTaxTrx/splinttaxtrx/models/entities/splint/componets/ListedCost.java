package com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint.componets;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@JsonFormat(shape = JsonFormat.Shape.ARRAY)
@JsonPropertyOrder({"card", "cost"})
public class ListedCost {
    private String card;
    private BigDecimal cost;
}
