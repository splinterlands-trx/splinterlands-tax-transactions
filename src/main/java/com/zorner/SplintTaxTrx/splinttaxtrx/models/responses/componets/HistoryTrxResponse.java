package com.zorner.SplintTaxTrx.splinttaxtrx.models.responses.componets;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
public class HistoryTrxResponse {
    @JsonProperty("trx_id")
    private String trxId;
    private long block;
    @JsonProperty("trx_in_block")
    private int trxInBlock;
    @JsonProperty("op_in_trx")
    private int opInTrx;
    @JsonProperty("virtual_op")
    private boolean virtualOp;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime timestamp;
    private OpResponse op;
    @JsonProperty("operation_id")
    private int operationId;
}
