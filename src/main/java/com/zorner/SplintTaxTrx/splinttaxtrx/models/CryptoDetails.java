package com.zorner.SplintTaxTrx.splinttaxtrx.models;

import com.zorner.SplintTaxTrx.splinttaxtrx.models.enums.CoinpandaTrxLabel;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.enums.CoinpandaTrxType;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class CryptoDetails {
    private CoinpandaTrxType type;
    private double sentAmount;
    private String sentCurrency;
    private double receivedAmount;
    private String receivedCurrency;
    private double feeAmount;
    private String feeCurrency;
    private CoinpandaTrxLabel label;
    private String description;
}