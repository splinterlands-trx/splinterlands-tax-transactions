package com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class SetAuthority extends AbstractSplint {
    private List<String> rental;
}
