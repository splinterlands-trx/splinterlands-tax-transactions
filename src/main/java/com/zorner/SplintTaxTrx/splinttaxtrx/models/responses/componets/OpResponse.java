package com.zorner.SplintTaxTrx.splinttaxtrx.models.responses.componets;

import lombok.Getter;

@Getter
public class OpResponse {
    private String type;
    private ValueResponse value;
}
