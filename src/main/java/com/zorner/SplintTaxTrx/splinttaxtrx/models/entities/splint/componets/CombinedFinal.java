package com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint.componets;

import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.CryptoTransaction;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class CombinedFinal implements Comparable<CombinedFinal> {
    private CryptoTransaction trx;
    private List<String> cards;
    
    @Override
    public int compareTo(CombinedFinal combinedFinal) {
        return this.trx.getDate().compareTo(combinedFinal.getTrx().getDate());
    }
}
