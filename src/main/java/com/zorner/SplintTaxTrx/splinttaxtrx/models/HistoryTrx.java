package com.zorner.SplintTaxTrx.splinttaxtrx.models;

import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint.AbstractSplint;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.enums.SplintOpType;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.List;

@Builder
@Getter
public class HistoryTrx<SplintDetail extends AbstractSplint> {
    private String txrId;
    private long block;
    private int trxInBlock;
    private int opInTrx;
    private boolean virtualOp;
    private LocalDateTime timestamp;
    private String opType;
    private List<String> requiredAuths;
    private List<String> requiredPostingAuths;
    private SplintOpType splintOpType;
    private SplintDetail details;
    private int operationId;
}