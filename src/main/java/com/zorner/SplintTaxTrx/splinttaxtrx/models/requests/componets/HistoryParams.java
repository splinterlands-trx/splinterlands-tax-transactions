package com.zorner.SplintTaxTrx.splinttaxtrx.models.requests.componets;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.Max;
import javax.validation.constraints.PositiveOrZero;

@Getter
@SuperBuilder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HistoryParams extends AbstractTransactionParams {
    @NonNull
    private String account;
    private int start;
    @Max(1000)
    private int limit;
    @PositiveOrZero
    @JsonProperty("operation_filter_low")
    private int operationFilterLow;
    @PositiveOrZero
    @JsonProperty("operation_filter_high")
    private int operationFilterHigh;
    
    @Override
    public String toString() {
        return "{ account='" + this.account + "', start=" + this.start + ", limit=" + this.limit +
                ", includeReversible=" + this.isIncludeReversible() + ", operationFilterLow=" + this.operationFilterLow +
                ", operationFilterHigh=" + this.operationFilterHigh + " }";
    }
}
