package com.zorner.SplintTaxTrx.splinttaxtrx.repositories;

import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.CryptoTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Repository
@Transactional
public interface TransactionsRepository extends JpaRepository<CryptoTransaction, Long> {
    @Query("SELECT trx FROM CryptoTransaction trx WHERE LENGTH(trx.receivedCurrency) > 11 AND trx.label = com.zorner.SplintTaxTrx.splinttaxtrx.models.enums.CoinpandaTrxLabel.NFT")
    List<CryptoTransaction> findNftPurchases();
    
    @Query("SELECT trx FROM CryptoTransaction trx WHERE trx.date > :after AND trx.date < :before")
    List<CryptoTransaction> findTrxesBetweenDates(LocalDateTime after, LocalDateTime before);
}