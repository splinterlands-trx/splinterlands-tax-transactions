package com.zorner.SplintTaxTrx.splinttaxtrx.services.support;

import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.CryptoTransaction;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

@Getter
@Setter(value = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SumAmounts {
    private BigDecimal sentSum;
    private BigDecimal receivedSum;
    private BigDecimal feeSum;
    private int count;
    
    public SumAmounts(CryptoTransaction trx) {
        this.sentSum = trx.getSentAmount();
        this.receivedSum = trx.getReceivedAmount();
        this.feeSum = trx.getFeeAmount();
        this.count = 1;
    }
    
    public SumAmounts addTransaction(CryptoTransaction trx) {
        if (this.sentSum != null)
            this.sentSum = this.sentSum.add(trx.getSentAmount());
        if (this.receivedSum != null)
            this.receivedSum = this.receivedSum.add(trx.getReceivedAmount());
        if (this.feeSum != null)
            this.feeSum = this.feeSum.add(trx.getFeeAmount());
        this.count++;
        return this;
    }
    
    public SumAmounts getPercentages(CryptoTransaction trx) {
        SumAmounts percentages = new SumAmounts();
        if (this.sentSum != null && !this.sentSum.equals(BigDecimal.valueOf(0L)))
            percentages.setSentSum(trx.getSentAmount().divide(this.sentSum, 5, RoundingMode.HALF_UP));
        if (this.receivedSum != null && !this.receivedSum.equals(BigDecimal.valueOf(0L)))
            percentages.setReceivedSum(trx.getReceivedAmount().divide(this.receivedSum, 5, RoundingMode.HALF_UP));
        if (this.feeSum != null && !this.feeSum.equals(BigDecimal.valueOf(0L)))
            percentages.setFeeSum(trx.getFeeAmount().divide(this.feeSum, 5, RoundingMode.HALF_UP));
        percentages.setCount(this.count);
        return percentages;
    }
    
    public boolean arePercentagesSimilar(SumAmounts percentages) {
        BigDecimal allowance = BigDecimal.valueOf(0.005);
        if (this.count != percentages.count)
            return false;
        return ((this.sentSum == null && percentages.sentSum == null) ||
                (percentages.sentSum != null && Objects.requireNonNull(this.sentSum).subtract(allowance).compareTo(percentages.sentSum) < 0 &&
                        this.sentSum.add(allowance).compareTo(percentages.sentSum) > 0)) &&
                ((this.receivedSum == null && percentages.receivedSum == null) ||
                        (percentages.receivedSum != null && Objects.requireNonNull(this.receivedSum).subtract(allowance).compareTo(percentages.receivedSum) < 0 &&
                                this.receivedSum.add(allowance).compareTo(percentages.receivedSum) > 0)) &&
                ((this.feeSum == null && percentages.feeSum == null) ||
                        (percentages.feeSum != null && Objects.requireNonNull(this.feeSum).subtract(allowance).compareTo(percentages.feeSum) < 0 &&
                                this.feeSum.add(allowance).compareTo(percentages.feeSum) > 0));
    }
}
