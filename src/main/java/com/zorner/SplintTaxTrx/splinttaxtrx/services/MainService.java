package com.zorner.SplintTaxTrx.splinttaxtrx.services;

import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.CryptoTransaction;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.SplintBalanceTrx;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.splint.CombineCards;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.enums.CoinpandaTrxLabel;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.requests.HistoryRequest;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.requests.componets.HistoryParams;
import com.zorner.SplintTaxTrx.splinttaxtrx.repositories.TransactionsRepository;
import com.zorner.SplintTaxTrx.splinttaxtrx.services.support.SumAmounts;
import com.zorner.SplintTaxTrx.splinttaxtrx.transformers.HiveTrxTransformer;
import com.zorner.SplintTaxTrx.splinttaxtrx.transformers.SplintTranformer;
import com.zorner.SplintTaxTrx.splinttaxtrx.utils.CsvUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

@Slf4j
@Component
public class MainService implements CommandLineRunner {
    
    @Autowired
    private SplintTranformer splintTranformer;
    
    @Autowired
    private HiveTrxTransformer hiveTrxTransformer;
    
    @Autowired
    private SplintRequests splintRequests;
    
    @Autowired
    private TransactionsRepository transactionsRepository;
    
    @Autowired
    private CsvUtils csvUtils;
    
    private String player = "";
    private String csvInputFile = "";
    private String cryptoTaxService;
    
    @SuppressWarnings("RedundantThrows")
    @Override
    public void run(String... args) throws Exception {
    
        if (args.length == 0) {
            System.out.println("Player's Name missing.");
            printHelp();
        } else {
            this.player = args[0];
        }
    
        if (args.length > 1)
            this.cryptoTaxService = args[1].toLowerCase(Locale.ROOT);
        else
            this.cryptoTaxService = "coinpanda";
    
        if (args.length > 2)
            this.csvInputFile = args[2];
        else
            this.csvInputFile = "splinterlands-balances.csv";
    
        loadCSV();
        getPlayerHistory();
    
        System.out.println("Writing CSV file: SplinterlandsCoinpanda.csv");
        log.info("Writing CSV file: SplinterlandsCoinpanda.csv");
        csvUtils.writeCsvList(CryptoTransaction.class, transactionsRepository.findAll(), "SplinterlandsCoinpanda.csv");
        System.out.println("CSV SplinterlandsCoinpanda.csv written. Closing program.");
        log.info("CSV SplinterlandsCoinpanda.csv written. Closing program.");
        
        System.exit(0);
    }
    
    private void printHelp() {
        System.out.println("Usage: SplintTaxTrxApplication playerName [CryptoTaxService] [CSV_filename] ");
        System.exit(1);
    }
    
    public void loadCSV() {
        List<SplintBalanceTrx> csvTrxes = csvUtils.getCsvList(SplintBalanceTrx.class, this.csvInputFile);
        List<CryptoTransaction> cryptoTrxes = splintTranformer.getTransactions(csvTrxes);
        System.out.println(this.csvInputFile + " is being loaded into the database.");
        log.info(this.csvInputFile + " is being loaded into the database.");
        for (CryptoTransaction trx : cryptoTrxes) {
            transactionsRepository.save(trx);
        }
        transactionsRepository.flush();
    
        System.out.println(this.csvInputFile + " has been loaded into the database successfully.");
        log.info(this.csvInputFile + " has been loaded into the database successfully.");
    }
    
    public void getPlayerHistory() {
        HistoryRequest historyRequest = new HistoryRequest().toBuilder()
                .params(HistoryParams.builder()
                        .account(this.player)
                        .start(-1)                      // Pull transactions in reverse order
                        .limit(1000)                    // Limit the number of transactions returned 1-1000 max
                        .includeReversible(true)       // Operations from reversible block included
                        .operationFilterLow(262144)   // Queries only custom jsons
                        .build())
                .build();
        List<CryptoTransaction> cryptoTrxes = hiveTrxTransformer.getCryptoTransactions(splintRequests.getTrxHistory(historyRequest));
        System.out.println("Loading history transactions into database.");
        log.info("Loading history transactions into database.");
        
        long allowance = 10L;
        List<CryptoTransaction> potentialTrxes;
        List<CryptoTransaction> similarTrxes = new LinkedList<>();
        List<CryptoTransaction> exactAmountTrxes = new LinkedList<>();
        Map<LocalDateTime, SumAmounts> historyAmountSums = new TreeMap<>();
        Map<LocalDateTime, SumAmounts> databaseAmountSums = new TreeMap<>();
        for (CryptoTransaction trx : cryptoTrxes) {
            if (historyAmountSums.containsKey(trx.getDate()))
                historyAmountSums.get(trx.getDate()).addTransaction(trx);
            else
                historyAmountSums.put(trx.getDate(), new SumAmounts(trx));
        }
        Map<LocalDateTime, List<CryptoTransaction>> usedTrxes = new HashMap<>();
        for (CryptoTransaction trx : cryptoTrxes) {
            potentialTrxes = transactionsRepository.findTrxesBetweenDates(trx.getDate().minusSeconds(allowance), trx.getDate().plusSeconds(allowance));
            similarTrxes.clear();
            for (CryptoTransaction potentialtrx : potentialTrxes) {
                if (potentialtrx.isSimilar(trx))
                    similarTrxes.add(potentialtrx);
            }
            exactAmountTrxes.clear();
            if (similarTrxes.size() > 0) {
                for (CryptoTransaction similarTrx : similarTrxes) {
                    if (similarTrx.areAmountsEqual(trx))
                        exactAmountTrxes.add(similarTrx);
                    else
                        log.info("Transaction amount not an exact match:" + System.lineSeparator() +
                                "transaction: " + trx + System.lineSeparator() + "In database: " + similarTrx);
                }
                if (similarTrxes.size() > 1 && !databaseAmountSums.containsKey(trx.getDate())) {
                    for (CryptoTransaction similarTrx : similarTrxes) {
                        if (databaseAmountSums.containsKey(trx.getDate()))
                            databaseAmountSums.get(trx.getDate()).addTransaction(similarTrx);
                        else
                            databaseAmountSums.put(trx.getDate(), new SumAmounts(similarTrx));
                    }
                }
                boolean notMatched = true;
                if (exactAmountTrxes.size() > 0) {
                    if (similarTrxes.size() > 1 && !usedTrxes.containsKey(trx.getDate()))
                        usedTrxes.put(trx.getDate(), new LinkedList<>());
                    for (CryptoTransaction exactAmountTrx : exactAmountTrxes) {
                        if (!usedTrxes.containsKey(trx.getDate()) || !usedTrxes.get(trx.getDate()).contains(exactAmountTrx)) {
                            transactionsRepository.save(updateTrx(exactAmountTrx, trx));
                            if (usedTrxes.containsKey(trx.getDate()))
                                usedTrxes.get(trx.getDate()).add(updateTrx(exactAmountTrx, trx));
                            notMatched = false;
                            break;
                        }
                    }
                    if (notMatched) {
                        transactionsRepository.save(trx);
                        log.warn("Exact transaction matches found, but none not updated already. This transaction being added as new. \nTraction: " + trx);
                        System.out.println("Exact transaction matches found, but none not updated already. This transaction being added as new. \nTraction: " + trx);
                    }
                } else {
                    SumAmounts targetPercents = new SumAmounts(trx);
                    if (similarTrxes.size() > 1) {
                        targetPercents = historyAmountSums.get(trx.getDate()).getPercentages(trx);
                        if (!usedTrxes.containsKey(trx.getDate())) {
                            usedTrxes.put(trx.getDate(), new LinkedList<>());
                        }
                    }
                    for (CryptoTransaction similarTrx : similarTrxes) {
                        if (!usedTrxes.containsKey(trx.getDate()) || !usedTrxes.get(trx.getDate()).contains(similarTrx)) {
                            if (similarTrxes.size() == 1 ||
                                    targetPercents.arePercentagesSimilar(databaseAmountSums.get(trx.getDate()).getPercentages(similarTrx))) {
                                transactionsRepository.save(updateTrx(similarTrx, trx));
                                if (usedTrxes.containsKey(trx.getDate()))
                                    usedTrxes.get(trx.getDate()).add(updateTrx(similarTrx, trx));
                                notMatched = false;
                                break;
                            }
                        }
                    }
                    if (notMatched) {
                        transactionsRepository.save(trx);
                        log.warn("Similar transaction found, but none not updated already or having the same percentages as this one. This transaction is being added. \nTraction: " + trx);
                        System.out.println("Similar transaction found, but none not updated already or having the same percentages as this one. This transaction is being added. \nTraction: " + trx);
                    }
                }
            } else if (trx.hasAmount()) {
                transactionsRepository.save(trx);
                log.info("No similar transaction found. Adding transaction. Maybe an unmatched transaction or an incomplete action that should be removed. \nTransaction: " + trx);
                System.out.println("No similar transaction found. Adding transaction. Maybe an unmatched transaction or an incomplete action that should be removed. \nTransaction: " + trx);
            } else
                log.info("Not saving 0 value transactions. \nTraction: " + trx);
        }
        transactionsRepository.flush();
        
        // Time dependant. This has to be run after all other data is in the DB.
        List<CryptoTransaction> finalTrxes = CombineCards.getCombinedTransaction(transactionsRepository.findNftPurchases());
        for (CryptoTransaction trx : finalTrxes) {
            transactionsRepository.save(trx);
        }
        System.out.println("History transactions loaded into database.");
        log.info("History transactions loaded into database.");
    }
    
    private CryptoTransaction updateTrx(CryptoTransaction current, CryptoTransaction newTrx) {
        if (current.getTrxHash() == null || (current.getTrxHash() != null && current.getTrxHash().isEmpty()))
            current.setTrxHash(newTrx.getTrxHash());
        if (current.getLabel().equals(CoinpandaTrxLabel.NFT)) {
            if (!newTrx.getSentCurrency().equals("USD"))
                current.setSentCurrency(newTrx.getSentCurrency());
            if (!newTrx.getReceivedCurrency().equals("USD"))
                current.setReceivedCurrency(newTrx.getReceivedCurrency());
            current.setDescription(newTrx.getDescription());
        }
        return current;
    }
}
