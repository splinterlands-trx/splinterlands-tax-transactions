package com.zorner.SplintTaxTrx.splinttaxtrx.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.requests.HistoryRequest;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.requests.TransactionRequest;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.responses.HistoryResponse;
import com.zorner.SplintTaxTrx.splinttaxtrx.models.responses.TransactionResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.Objects;

@Slf4j
@Service
public class SplintRequests {
    
    private final RestTemplate restTemplate;
    private final String url = "https://api.hive.blog";
    private final HttpHeaders headers = new HttpHeaders();
    private final ObjectMapper objectMapper = new ObjectMapper();
    
    public SplintRequests() {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        // set connection and read timeouts
        this.restTemplate = restTemplateBuilder
                .setConnectTimeout(Duration.ofSeconds(500))
                .setReadTimeout(Duration.ofSeconds(500))
                .build();
        this.headers.setContentType(MediaType.APPLICATION_JSON);
    }
    
    public HistoryResponse getTrxHistory(HistoryRequest historyRequest) {
        ResponseEntity<HistoryResponse> resp = null;
        System.out.println("Requesting history transactions for " + historyRequest.getParams().getAccount() + " from " + url + ".");
        log.info("Requesting history transactions for " + historyRequest.getParams().getAccount() + " from " + url + ". Request: " + historyRequest);
        try {
            resp = this.restTemplate.postForEntity(url, new HttpEntity<>(objectMapper.writeValueAsString(historyRequest), headers), HistoryResponse.class);
        } catch (JsonProcessingException e) {
            log.info("Parsing error on: " + historyRequest);
            e.printStackTrace();
        }
        System.out.println("History transactions response received.");
        log.info("History transactions response received. StatusCode: " + Objects.requireNonNull(resp).getStatusCode());
        return Objects.requireNonNull(resp).getStatusCode() == HttpStatus.OK ? resp.getBody() : null;
    }
    
    public TransactionResponse getTransaction(TransactionRequest transactionRequest) {
        log.info("Requesting transaction for from " + url + ". Request: " + transactionRequest.toString());
        ResponseEntity<TransactionResponse> resp = null;
        try {
            resp = this.restTemplate.postForEntity(url, new HttpEntity<>(objectMapper.writeValueAsString(transactionRequest), headers), TransactionResponse.class);
        } catch (JsonProcessingException e) {
            log.info("Parsing error on: " + transactionRequest);
            e.printStackTrace();
        }
        log.info("Transactions response received. StatusCode: " + Objects.requireNonNull(resp).getStatusCode());
        return Objects.requireNonNull(resp).getStatusCode() == HttpStatus.OK ? resp.getBody() : null;
    }
}
