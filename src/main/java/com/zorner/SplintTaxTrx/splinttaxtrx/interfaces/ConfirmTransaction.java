package com.zorner.SplintTaxTrx.splinttaxtrx.interfaces;

import com.zorner.SplintTaxTrx.splinttaxtrx.TransactionValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {TransactionValidator.class})
public @interface ConfirmTransaction {
    
    String message() default "Transaction incorrect for type give.";
    
    Class<?>[] groups() default {};
    
    Class<? extends Payload>[] payload() default {};
}
