package com.zorner.SplintTaxTrx.splinttaxtrx.interfaces;

import com.zorner.SplintTaxTrx.splinttaxtrx.models.entities.CryptoTransaction;

import java.util.List;

public interface SplintTrxInerface {
    List<CryptoTransaction> getCryptoDetail(CryptoTransaction trx);
}
